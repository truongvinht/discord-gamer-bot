[![Build Status](https://travis-ci.org/truongvinht/discord-gamer-bot.svg?branch=master)](https://travis-ci.org/truongvinht/discord-gamer-bot)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

# discord-gamer-bot
Discord Bot for displaying game related details and interact with players.

## Requirement
- Discord Server (Token)
- Server for deploying bot (NPM/NodeJs)



# License
MIT License (MIT)